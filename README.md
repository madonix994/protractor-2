# Protractor 2


Install:


npm install protractor

npm install chai

npm install fastcsv

npm install faker


Run regression test suite:

protractor .\conf.js --cucumberOpts.tags="@regression"
