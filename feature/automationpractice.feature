Feature: Automation practice site

    Background: Navigate
        Given User navigates to url: "http://automationpractice.com/index.php"

    @regression
    Scenario: Contact us
        Given User navigates to Contact us page
        When User select subject
        Then User adds email: "email@email.com" and order reference: "A100000"
        And User attaches file
        And User adds message
        And User sends message
        And User should see ok message: "Your message has been successfully sent to our team."



