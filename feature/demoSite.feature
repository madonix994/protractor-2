Feature: Demo site

    Background: Navigate
        Given User navigates to url: "https://www.seleniumeasy.com/test/"
        When User clicks Start Practising button

    @regression
    Scenario: Simple Form Demo
        Given User navigates to: "Simple Form Demo"
        When User enters message: "Hello world!"
        Then User clicks Show Message button
        And User expects to see message: "Hello world!"

    @regression
    Scenario: Simple Form Demo 2
        Given User navigates to: "Simple Form Demo"
        When User enters values: "500" and "300"
        Then User clicks Get Total button
        And User expects to see value: "800"

    @regression
    Scenario: Check Box Demo
        Given User navigates to: "Check Box Demo"
        When User selects check box
        Then User should see message: "Success - Check box is checked"

    @regression
    Scenario: Radio buttons Demo
        Given User navigates to: "Radio Buttons Demo"
        When User selects radio button
        Then User selects second radio button
        And User clicks Get Values

    @regression
    Scenario: Select Dropdown List
        Given User navigates to: "Select Dropdown List"
        When User selects day: "Friday"
        Then User should see message below: "Day selected :- Friday"

    @regression
    Scenario: Javascript Alerts
        Given User navigates to: "Javascript Alerts"
        When User clicks first alert button
        Then User clicks OK
        When User clicks second alert box button
        Then User clicks Cancel
        When User clicks third alert box button
        Then User enters name: "Predrag Kolovic"

    @regression
    Scenario: Input form with validations
        Given User clicks Proceed Next link
        When User navigates to: "Input Form with Validations"
        Then User adds name: "name" and surname: "surname"
        And User adds email: "email@email.com" and phone: "381654442828"
        And User adds address: "Address 17" and city: "Guberevac"
        And User adds state: "Oregon" and zip code: "36000"
        And User adds website: "www.website.com" and selects no for hosting
        And User adds comment: "Comment comment  comment  comment  comment  comment  comment  comment  comment  comment  comment."
        And User submits the form

    @disabled
    #Forma se ne popunjava zbog sendKeys() -> TypeError: each key must be a number of string; got undefined
    Scenario: Ajax Form Submit
        Given User clicks Proceed Next link
        When User navigates to: "Ajax Form Submit"
        Then User user adds name: "Name Surname" and comment: "Comment comment  comment  comment  comment  comment  comment  comment  comment  comment  comment."

    @regression
    Scenario: JQuery Select dropdown
        Given User clicks Proceed Next link
        When User navigates to: "JQuery Select dropdown"
        Then User adds country: "India"
        And User adds state: "Alaska"

    @regression
    Scenario: Bootstrap List Box
        Given User clicks Proceed Next link
        When User navigates to: "Bootstrap List Box"
        Then User moves one element from left box to right box

    @regression
    Scenario: JQuery List Box
        Given User clicks Proceed Next link
        When User navigates to: "JQuery List Box"
        Then User adds one element to right list box
        And User removes all elements from right list box
        And User adds all elements to the right list box

    @regression
    Scenario: Data List Filter
        Given User clicks Proceed Next link
        When User navigates to: "Data List Filter"
        Then User searches for person by name: "Brenda"

    @regression
    Scenario: File Download
        Given User clicks Proceed Next link
        When User navigates to: "File Download"
        Then User generates 500 characters
    # And User Downloads file

    @regression
    Scenario: Table Pagination
        Given User clicks Proceed Next link
        When User clicks Proceed Next Advanced link
        Then User navigates to: "Table Pagination"
        And User navigates to second page
        And User navigates to last page

    @regression
    Scenario: Table Data Search
        Given User clicks Proceed Next link
        When User clicks Proceed Next Advanced link
        Then User navigates to: "Table Data Search"
        And User searches for person: "failed qa"

    @disabled
    #Ne radi mi drag and drop
    Scenario: Drag and Drop
        Given User clicks Proceed Next link
        When User clicks Proceed Next Advanced link
        Then User navigates to: "Drag and Drop"
        And User drags and drops all elements





