Feature: Register
    Scenario Outline: Fill registration form
        Given User navigates to url: "http://demo.automationtesting.in/Register.html"
        Then User enters full name "<firstName>" "<lastName>"
        Then User enters "<email>" mail address
        Then User enters "<address>" home address
        Then User enters "<phone>" phone number
        Then User chose "<gender>" gender
        Then User chose "<hobbie>" hobbie
        Then User enters "<language>" language
        Then User enters "<skill>" skill
        Then User enters "<country>" country
        Then User chose "<selectedCountry>" country
        Then User chose date of birth "<year>" "<month>" "<day>"
        Then User enters "<password>" password and repeats
        Then User submits the registration form

@regression
        Examples:
            | firstName | lastName | email         | address            | phone      | gender | hobbie  | language | skill   | country | selectedCountry | year | month    | day | password  |
            | Joe       | Gomez    | test@test.com | Narodnog Heroja 12 | 0611553311 | Male   | Movies  | English  | Android | Japan   | Denmark         | 1995 | November | 12  | Password1 |
            | Elizabet  | Taylor   | test@test.com | Narodnog Heroja 14 | 0611553311 | Female | Cricket | English  | Android | Jamaica | Japan           | 1995 | December | 12  | Password1 |
