let helper = require('../helper/helper.js');
let util = require('../helper/util.js');
let expect = require('chai').expect;
let path = require('path');

let autoPractPOM = function () {

    let contactUsLink = element(by.xpath("//a[.='Contact us']"));
    let subjectSelect = element(by.xpath("//select[@class='form-control']"));
    let emailInput = element(by.id('email'));
    let orderInput = element(by.id('id_order'));
    let inputMessage = element(by.id('message'));
    let submitButton = element(by.xpath("//span[text()='Send']"));

    let dressesLink = element(by.xpath("(//a[@title='Dresses'])[2]"));
    let dress = element(by.xpath("//img[@alt='Printed Chiffon Dress']"));

    this.navigateToContactUsPage = function () {
        contactUsLink.click();
    }

    this.selectSubject = function () {
        subjectSelect.click().element(by.css("option[value='2']")).click();
    }

    this.addEmail = function (email) {
        emailInput.sendKeys(email);
    }

    this.addOrder = function (order) {
        orderInput.sendKeys(order);
    }

    this.attachFile = function () {
        var absolutePath = path.resolve('D:/filename.txt');
        var fileElem = element(by.id('fileUpload'));
        fileElem.sendKeys(absolutePath);
    }

    this.addMessage = function () {
        let message = '';
        let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let charactersLength = characters.length;
        for (let i = 0; i < 500; i++) {
            message += characters.charAt(Math.floor(Math.random() * charactersLength));
        }

        inputMessage.sendKeys(message);
    }

    this.sendMessage = function () {
        submitButton.click();
    }

    this.validateMessage = async function (message) {
        let actualMessage = element(by.xpath("//p[@class='alert alert-success']"));
        await actualMessage.getText().then(function (actualValue) {
            expect(actualValue).to.equal(message);
        });
    }

}
module.exports = new autoPractPOM();
