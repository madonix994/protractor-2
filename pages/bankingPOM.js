let expect = require('chai').expect;
let helper = require('../helper/helper.js');
let util = require('../helper/util.js');

let bankingPOM = function () {

    let loginButton = element(by.xpath("//button[.='Customer Login']"));
    let selectedName = element(by.id('userSelect')).element(by.css("option[value='1']"));
    let depositButton = element(by.css("button[ng-click='deposit()']"));


    this.clickTheLoginButton = function () {
        loginButton.click();
        loginButton = element(by.xpath("//button[@class='btn btn-default']"));
    }

    this.selectName = function (name) {
        selectedName.click();
    }

};

module.exports = new bankingPOM();