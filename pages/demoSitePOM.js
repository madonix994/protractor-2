let helper = require('../helper/helper.js');
let util = require('../helper/util.js');
let expect = require('chai').expect;
let fs = require('fs');
let csv = require('fast-csv');
let ws = fs.createWriteStream('./work/MY_CSV.csv');

let demoSitePOM = function () {

    let startPractisingButton = element(by.id('btn_basic_example'));
    let userMessage = element(by.xpath("//input[@id='user-message']"));
    let showMessageButton = element(by.xpath("//button[.='Show Message']"));
    let fisrtInputValue = element(by.id('sum1'));
    let secondInputValue = element(by.id('sum2'));
    let getTotalButton = element(by.xpath("//button[.='Get Total']"));
    let checkBoxDemo = element(by.xpath("//a[.='Check Box Demo']"));
    let checkBox = element(by.id('isAgeSelected'));
    let firstRadioButton = element(by.css('input[name="gender"][value="Male"]'));
    let secondRadioButton = element(by.css('input[name="ageGroup"][value="15 - 50"]'));
    let getValues = element(by.xpath("//button[.='Get values']"));
    let firstAlertBoxButton = element(by.css("button[onclick='myAlertFunction()']"));
    let secondAlertBoxButton = element(by.css("button[onclick='myConfirmFunction()']"));
    let thirdAlertBoxButton = element(by.css("button[onclick='myPromptFunction()']"));
    let datePickersLink = element(by.xpath("//a[.='Date pickers']"));
    let boostrapDatePickerLink = element(by.xpath("//ul[@id='treemenu']//a[.='Bootstrap Date Picker']"));
    let proceedNextLink = element(by.id('btn_inter_example'));
    let proceedNextLinkAdvanced = element(by.id('btn_advanced_example'));

    let firstNameInput = element(by.name('first_name'));
    let lastNameInput = element(by.name('last_name'));
    let emailInput = element(by.name('email'));
    let phoneInput = element(by.name('phone'));
    let addressInput = element(by.name('address'));
    let cityInput = element(by.name('city'));
    let stateInput = element(by.name('email'));
    let zipInput = element(by.name('zip'));
    let websiteInput = element(by.name('website'));
    let hostingInput = element(by.css('input[name="hosting"][value="no"]'));
    let commentInput = element(by.name('comment'));
    let submitButton = element(by.xpath("//button[@class='btn btn-default']"));

    let nameInputAjax = element(by.id('title'));
    let commentInputAjax = element(by.id('description'));

    let countryInput = element(by.css("span[aria-labelledby='select2-country-container']"));
    let stateInputNew = element(by.css("[placeholder='Select state(s)']"));

    let moveElement = element(by.xpath("//li[contains(.,'bootstrap-duallist')]"));
    let moveRight = element(by.xpath("//span[@class='glyphicon glyphicon-chevron-right']"));

    let leftElement = element(by.xpath('//*[@id="pickList"]/div/div[1]/select/option[2]'));
    let addButton = element(by.xpath("//button[@class='pAdd btn btn-primary btn-sm']"));
    let removeAll = element(by.xpath("//button[@class='pRemoveAll btn btn-primary btn-sm']"));
    let addAll = element(by.xpath("//button[@class='pAddAll btn btn-primary btn-sm']"));

    let inputSearch = element(by.id('input-search'));

    let inputMessage = element(by.id('textbox'));
    let createButton = element(by.id('create'));
    let downloadLink = element(by.id('link-to-download'));

    let pageTwoButton = element(by.xpath("//a[.='2']"));
    let nextPageButton = element(by.xpath("//a[.='»']"));

    let searchForPerson = element(by.id('task-table-filter'));

    let firstElement = element(by.xpath("//span[.='Draggable 1']"));
    let secondElement = element(by.xpath("//span[.='Draggable 2']"));
    let thirdElement = element(by.xpath("//span[.='Draggable 3']"));
    let fourthElement = element(by.xpath("//span[.='Draggable 4']"));
    let destination = element(by.id('mydropzone'));


    this.clickStartPractisingButton = function () {
        startPractisingButton.click();
    }

    this.clickSimpleFormDemo = function () {
        simpleFormDemoLink.click();
    }

    this.enterMessage = function (message) {
        userMessage.sendKeys(message);
    }

    this.clickShowMessage = function () {
        showMessageButton.click();
    }

    this.validateMessage = async function (message) {
        let actualMessage = element(by.id('display'));
        await actualMessage.getText().then(function (actualValue) {
            expect(actualValue).to.equal(message);
        });
    }

    this.enterValues = function (first, second) {
        fisrtInputValue.sendKeys(first);
        secondInputValue.sendKeys(second);
    }

    this.clickGetTotalButton = function () {
        getTotalButton.click();
    }

    this.validateResult = async function (expectedResult) {
        let actualValue = element(by.id('displayvalue'));
        await actualValue.getText().then(function (actualResult) {
            expect(actualResult).to.equal(expectedResult);
        });
    }

    this.clickCheckBoxDemo = function () {
        checkBoxDemo.click();
    }

    this.selectCheckBox = function () {
        checkBox.click();
    }

    this.validateCheckBoxMessage = async function (message) {
        let actualMessage = element(by.id('txtAge'));
        await actualMessage.getText().then(function (actualValue) {
            expect(actualValue).to.equal(message);
        });
    }

    this.navigateToPage = function (page) {
        switch (page) {
            case "Simple Form Demo":
                element(by.xpath("//div[@id='basic']//a[.='" + page + "']")).click();
                break;
            case "Check Box Demo":
                element(by.xpath("//div[@id='basic']//a[.='" + page + "']")).click();
                break;
            case "Radio Buttons Demo":
                element(by.xpath("//div[@id='basic']//a[.='" + page + "']")).click();
                break;
            case "Select Dropdown List":
                element(by.xpath("//div[@id='basic']//a[.='" + page + "']")).click();
                break;
            case "Javascript Alerts":
                element(by.xpath("//div[@id='basic']//a[.='" + page + "']")).click();
                break;
            case "Input Form with Validations":
                element(by.xpath("//a[.='" + page + "']")).click();
                break;
            case "Ajax Form Submit":
                element(by.xpath("//div[@id='intermediate']//a[.='" + page + "']")).click();
                break;
            case "JQuery Select dropdown":
                element(by.xpath("//div[@id='intermediate']//a[.='" + page + "']")).click();
                break;
            case "Bootstrap List Box":
                element(by.xpath("//div[@id='intermediate']//a[.='" + page + "']")).click();
                break;
            case "JQuery List Box":
                element(by.xpath("//div[@id='intermediate']//a[.='" + page + "']")).click();
                break;
            case "Data List Filter":
                element(by.xpath("//div[@id='intermediate']//a[.='" + page + "']")).click();
                break;
            case "File Download":
                element(by.xpath("//div[@id='intermediate']//a[.='" + page + "']")).click();
                break;
            case "Table Pagination":
                element(by.xpath("//div[@id='advanced']//a[.='" + page + "']")).click();
                break;
            case "Table Data Search":
                element(by.xpath("//div[@id='advanced']//a[.='" + page + "']")).click();
                break;
            case "Drag and Drop":
                element(by.xpath("//div[@id='advanced']//a[.='" + page + "']")).click();
                break;

            default:
                console.log("Page: " + page + " doesn't exist on this site!");
                break;
        }
    }

    this.selectRadioButon = function () {
        firstRadioButton.click();
    }

    this.selectSecondRadioButton = function () {
        secondRadioButton.click();
    }

    this.clickGetValues = async function () {
        getValues.click();
    }

    this.selectDay = function (day) {
        element(by.id('select-demo')).click().element(by.css("option[value='" + day + "']")).click();
    }

    this.validateMessageBelow = async function (message) {
        let actualMessage = element(by.xpath("//p[@class='selected-value']"));
        await actualMessage.getText().then(function (actualValue) {
            expect(actualValue).to.equal(message);
        });
    }

    this.clickfirstAlertBoxButton = function () {
        firstAlertBoxButton.click();
    }

    this.clickOK = async function () {
        await browser.switchTo().alert().accept();
    }

    this.clickSecondAlertBoxButton = function () {
        secondAlertBoxButton.click();
    }

    this.clickCancel = async function () {
        await browser.switchTo().alert().dismiss();
    }

    this.clickThirdAlertBoxButton = function () {
        thirdAlertBoxButton.click();
    }

    this.enterName = async function (name) {
        await browser.switchTo().alert().sendKeys(name);
        helper.sleep(1000);
        await browser.switchTo().alert().accept();
    }

    this.clickDatePickersLink = function () {
        datePickersLink.click();
        helper.sleep(5000);
        boostrapDatePickerLink.click();
    }

    this.clickProceedNext = function () {
        proceedNextLink.click();
    }

    this.addNameAndSurname = function (name, surname) {
        firstNameInput.sendKeys(name);
        lastNameInput.sendKeys(surname);
    }

    this.addEmailAndPhone = function (email, phone) {
        emailInput.sendKeys(email);
        phoneInput.sendKeys(phone);
    }

    this.addAddressAndCity = function (address, city) {
        addressInput.sendKeys(address);
        cityInput.sendKeys(city);
    }

    this.addStateAndZipCode = function (state, zip) {
        stateInput.click().sendKeys(state);
        zipInput.sendKeys(zip);
    }

    this.addWebsite = function (website) {
        websiteInput.sendKeys(website);
    }

    this.selectNoForHosting = function () {
        hostingInput.click();
    }

    this.addComment = function (comment) {
        commentInput.sendKeys(comment);
    }

    this.submitForm = function () {
        submitButton.click();
    }

    this.addNameAndComment = function (name, comment) {
        nameInputAjax.sendKeys(name);
        commentInputAjax.sendKeys(comment);
    }

    this.addCountry = async function (country) {
        countryInput.click().element(by.xpath("//span[@class='select2-search select2-search--dropdown']/input[@class='select2-search__field']")).sendKeys(country);
        await browser.actions().sendKeys(protractor.Key.ENTER).perform();
    }

    this.addState = async function (state) {
        stateInputNew.click().sendKeys(state);
        await browser.actions().sendKeys(protractor.Key.ENTER).perform();
    }

    this.moveFromLeftToRight = function () {
        moveElement.click();
        helper.sleep(1000);
        moveRight.click();
    }

    this.addElementRight = function () {
        leftElement.click();
        helper.sleep(1000);
        addButton.click();
    }

    this.removeAllElements = function () {
        removeAll.click();
    }

    this.addAllElements = function () {
        addAll.click();
    }

    this.findPerson = function (person) {
        inputSearch.click().sendKeys(person);
    }

    // If I need it
    this.saveToCsv = function () {
        fs.unlink('./work/MY_CSV.csv', function (err) {
            if (err) {
                console.log("Error while deleting the file " + err);
            }
        });

        csv.write([
            ["a1", "a2"],
            ["b1", "b2"]
        ], { headers: true })
            .pipe(ws);
    }

    this.generateCharacters = function () {
        let message = '';
        let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let charactersLength = characters.length;
        for (let i = 0; i < 500; i++) {
            message += characters.charAt(Math.floor(Math.random() * charactersLength));
        }

        inputMessage.sendKeys(message);
        createButton.click();
    }

    this.downloadFile = function () {
        downloadLink.click();
    }

    this.proceedNextAdvanced = function () {
        proceedNextLinkAdvanced.click();
    }

    this.navigateToSecondPage = function () {
        pageTwoButton.click();
    }

    this.navigateToLastPage = function () {
        nextPageButton.click();
    }

    this.searchForPersonInTable = function (person) {
        searchForPerson.click().sendKeys(person);
    }

    this.dragAndDropElements = async function () {
        await browser.actions().dragAndDrop(firstElement, destination).perform();
    }


}
module.exports = new demoSitePOM();