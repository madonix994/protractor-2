let helper = require('../helper/helper.js');
let util = require('../helper/util.js');

let registerPOM = function () {

    let passwordInput = element(by.model('Password'))
    let repeatPasswordInput = element(by.model('CPassword'))

    let submitBtn = element(by.id('submitbtn'))
    let refreshBtn = element(by.id('Button1'))

    let firstNameInput = element(by.model('FirstName'));
    let lastNameInput = element(by.model('LastName'));
    let adressInput = element(by.model('Adress'));
    let emailAdressInput = element(by.model('EmailAdress'));
    let phoneInput = element(by.model('Phone'));

    let genderList = element.all(by.model('radiovalue'));
    let languageField = element(by.id('msdd'));
    let skillField = element.all(by.model('Skill'));
    let countrySelectField = element(by.id('countries'));

    let yearField = element(by.model('yearbox'));
    let monthField = element(by.model('monthbox'));
    let dayField = element(by.model('daybox'));

    this.getURL = function (url) {
        browser.driver.get(url);
    }

    this.choseBirthDate = function (year, month, day) {
        util.utilElementClick(yearField);
        helper.findElementByOptionText(year).click();

        util.utilElementClick(monthField);
        helper.findElementByOptionText(month).click();

        util.utilElementClick(dayField);
        helper.findElementsByOptionText(day).click();
    }

    this.enterCountry = function (country) {
        let countryList = helper.findElementsByOptionText(country);
        countryList.get(1).click();
    }

    this.choseCountry = function (country) {
        util.utilElementClick(countrySelectField);
        helper.findElementByOptionText(country).click();
        util.utilElementClick(countrySelectField);
    }

    this.choseLanguage = function (language) {
        util.utilElementClick(languageField);
        helper.findElementByLinkText(language).click();
    }

    this.choseSkill = function (skill) {
        util.utilElementClick(skillField);
        helper.findElementByOptionText(skill).click();
    }

    this.choseGender = function (gender) {
        helper.waitForElementPresent(genderList);
        if (gender == 'Male') {
            genderList.get(0).click();
        } else if (gender == 'Female') {
            genderList.get(1).click();
        }
    }

    this.choseHobbie = function (hobbie) {
        if (hobbie == 'Cricket') {
            element(by.id('checkbox1'))
                .click();
        } else if (hobbie == 'Movies') {
            element(by.id('checkbox2'))
                .click();
        } else if (hobbie == 'Hokey') {
            element(by.id('checkbox3'))
                .click();
        }
    }

    this.enterEmail = function (email) {
        util.utilElementSendText(emailAdressInput, email);
    }

    this.enterAdress = function (adress) {
        util.utilElementSendText(adressInput, adress);
    }

    this.enterPasswordAndRepeat = function (password) {
        util.utilElementSendText(passwordInput, password);
        util.utilElementSendText(repeatPasswordInput, password);
    }

    this.enterFirstName = function (firstName) {
        util.utilElementSendText(firstNameInput, firstName);
    }

    this.enterLastName = function (lastName) {
        util.utilElementSendText(lastNameInput, lastName);
    }

    this.enterPhone = function (phone) {
        util.utilElementSendText(phoneInput, phone);
    }

    this.clickSubmitBtn = function () {
        util.utilElementClick(submitBtn);
    };
};

module.exports = new registerPOM();