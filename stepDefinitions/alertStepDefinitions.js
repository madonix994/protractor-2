var { Given, Then, When } = require('cucumber');
var { setDefaultTimeout } = require('cucumber');
setDefaultTimeout(60 * 1000);
let alertPOM = require('../pages/alertPOM.js');


When('User slects alert with OK button', async function () {// 
alertPOM.selectAlertOKCancelTab();
await browser.sleep(3000);
});


Then('User clicks to show alert with text box', async function () {// 
    alertPOM.selectAlertWithTextboxBtn();
    await browser.sleep(2000);
});


Then('User enters {string} in text box', async function (name) {// 
    browser.switchTo().alert().sendKeys(name);
    await browser.sleep(2000);
});


Then('User clicks to show alert', async function () {// 
    alertPOM.selectAlertOKCancelBtn();
    await browser.sleep(2000);
});


Then('User clicks on OK button on alert', async function () {// 
    browser.switchTo().alert().accept();
    await browser.sleep(2000);
});


Then('User should see message {string}', async function (confirmMsg) {// 
    let msg =  alertPOM.checkIfMessagePresent();
    await msg.then(function (actual) {
        expect(actual).to.equal(confirmMsg, 'Expected is:' + confirmMsg + ' Actual is:' + actual);
    });
    await browser.sleep(2000);
});

When('User slects alert with text box', async function () {// 
    alertPOM.selectAlertWithTextbox();
    await browser.sleep(3000);
    });

Then('User should see greeting message {string} {string} {string}', async function (msgPart1, name, msgPart2) {// 
    let msg =  alertPOM.checkIfGreetingMessagePresent();
    await msg.then(function (actual) {
        expect(actual).to.equal(msgPart1+" "+name+" "+msgPart2, 'Expected is:' + msgPart1+" "+name+" "+msgPart2 + ' Actual is:' + actual);
    }); 
    await browser.sleep(2000);
});