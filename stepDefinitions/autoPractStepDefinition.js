var { Given, Then, When } = require('cucumber');
var And = Then;
var { setDefaultTimeout } = require('cucumber');
setDefaultTimeout(60 * 1000);
let autoPractPOM = require('../pages/autoPractPOM');


Given('User navigates to Contact us page', async function () {
    autoPractPOM.navigateToContactUsPage();
    await browser.sleep(3000);
});

When('User select subject', async function () {
    autoPractPOM.selectSubject();
    await browser.sleep(3000);
});

Then('User adds email: {string} and order reference: {string}', async function (email, order) {
    autoPractPOM.addEmail(email);
    await browser.sleep(3000);
    autoPractPOM.addOrder(order);
    await browser.sleep(3000);
});

Then('User attaches file', async function () {
    autoPractPOM.attachFile();
    await browser.sleep(3000);
});

Then('User adds message', async function () {
    autoPractPOM.addMessage();
    await browser.sleep(3000);
});

Then('User sends message', async function () {
    autoPractPOM.sendMessage();
    await browser.sleep(3000);
});

Then('User should see ok message: {string}', async function (message) {
    autoPractPOM.validateMessage(message);
    await browser.sleep(2000);
});

