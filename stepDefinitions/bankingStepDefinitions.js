var { Given, Then, When } = require('cucumber');
var { setDefaultTimeout } = require('cucumber');
setDefaultTimeout(60 * 1000);
let bankingPOM = require('../pages/bankingPOM.js');


When('User clicks the login button', async function () {
    await browser.sleep(1000);
    bankingPOM.clickTheLoginButton();
    await browser.sleep(3000);
});

Then('User selects name: {string}', async function (name) {
    bankingPOM.selectName(name);
    await browser.sleep(3000);
});
