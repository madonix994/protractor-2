var { Given, Then, When } = require('cucumber');
var { setDefaultTimeout } = require('cucumber');
setDefaultTimeout(60 * 1000);
let calculatorPOM = require('../pages/calculatorPOM.js');
let helper = require('../helper/helper.js');


When('User enters first number {string} and second {string}', async function (firstNumber, secondNumber) {
    calculatorPOM.choseNumbers(firstNumber, secondNumber);
});

Then('User selects operation {string} to be performed', async function (operation) {
    calculatorPOM.choseOperation(operation);
});

Then('User select submit button', async function () {
    calculatorPOM.choseSubmit();
    await browser.sleep(3000);
});


Then('User verify the result to be {string}', async function (result) {
    let output = calculatorPOM.getResult(result);
    await output.then(function (actual) {
        expect(actual).to.equal(result, 'Expected is:' + result + ' Actual is:' + actual);
    });
});
