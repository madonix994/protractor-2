var { Given, Then, When } = require('cucumber');
var And = Then;
var { setDefaultTimeout } = require('cucumber');
setDefaultTimeout(60 * 1000);
let demoSitePOM = require('../pages/demoSitePOM.js');


When('User clicks Start Practising button', async function () {
    demoSitePOM.clickStartPractisingButton();
    await browser.sleep(3000);
});

When('User enters message: {string}', async function (message) {
    demoSitePOM.enterMessage(message);
    await browser.sleep(3000);
});

Then('User clicks Show Message button', async function () {
    demoSitePOM.clickShowMessage();
    await browser.sleep(3000);
});

When('User expects to see message: {string}', async function (message) {
    demoSitePOM.validateMessage(message);
    await browser.sleep(3000);
});

When('User enters values: {string} and {string}', async function (first, second) {
    demoSitePOM.enterValues(first, second);
    await browser.sleep(3000);
});

Then('User clicks Get Total button', async function () {
    demoSitePOM.clickGetTotalButton();
    await browser.sleep(3000);
});

Then('User expects to see value: {string}', async function (result) {
    demoSitePOM.validateResult(result);
    await browser.sleep(3000);
});

When('User selects check box', async function () {
    demoSitePOM.selectCheckBox();
    await browser.sleep(3000);
});

When('User should see message: {string}', async function (message) {
    demoSitePOM.validateCheckBoxMessage(message);
    await browser.sleep(3000);
});

Given('User navigates to: {string}', async function (page) {
    demoSitePOM.navigateToPage(page);
    await browser.sleep(3000);
});

When('User selects radio button', async function () {
    demoSitePOM.selectRadioButon();
    await browser.sleep(3000);
});

Then('User selects second radio button', async function () {
    demoSitePOM.selectSecondRadioButton();
    await browser.sleep(3000);
});

Then('User clicks Get Values', async function () {
    demoSitePOM.clickGetValues();
    await browser.sleep(3000);
});

When('User selects day: {string}', async function (day) {
    demoSitePOM.selectDay(day);
    await browser.sleep(3000);
});

Then('User should see message below: {string}', async function (message) {
    demoSitePOM.validateMessageBelow(message);
    await browser.sleep(3000);
});

When('User clicks first alert button', async function () {
    demoSitePOM.clickfirstAlertBoxButton();
    await browser.sleep(3000);
});

Then('User clicks OK', async function () {
    demoSitePOM.clickOK();
    await browser.sleep(3000);
});

Then('User clicks second alert box button', async function () {
    demoSitePOM.clickSecondAlertBoxButton();
    await browser.sleep(3000);
});

Then('User clicks Cancel', async function () {
    demoSitePOM.clickCancel()
    await browser.sleep(3000);
});

Then('User clicks third alert box button', async function () {
    demoSitePOM.clickThirdAlertBoxButton();
    await browser.sleep(3000);
});

Then('User enters name: {string}', async function (name) {
    demoSitePOM.enterName(name);
    await browser.sleep(3000);
});

Given('User navigates to Date pickers', async function () {
    demoSitePOM.clickDatePickersLink();
    await browser.sleep(3000);
});

Given('User clicks Proceed Next link', async function () {
    demoSitePOM.clickProceedNext();
    await browser.sleep(3000);
});

When('User clicks Proceed Next Advanced link', async function () {
    demoSitePOM.proceedNextAdvanced();
    await browser.sleep(3000);});

Then('User adds name: {string} and surname: {string}', async function (name, surname) {
    demoSitePOM.addNameAndSurname(name, surname);
});

Then('User adds email: {string} and phone: {string}', async function (email, phone) {
    demoSitePOM.addEmailAndPhone(email, phone);
});

Then('User adds address: {string} and city: {string}', async function (address, city) {
    demoSitePOM.addAddressAndCity(address, city);
    await browser.sleep(3000);});

Then('User adds state: {string} and zip code: {string}', async function (state, zip) {
    demoSitePOM.addStateAndZipCode(state, zip);
    await browser.sleep(3000);});

Then('User adds website: {string} and selects no for hosting', async function (website) {
    demoSitePOM.addWebsite(website);
    demoSitePOM.selectNoForHosting();
    await browser.sleep(3000);});

Then('User adds comment: {string}', async function (comment) {
    demoSitePOM.addComment(comment);
    await browser.sleep(3000);});

Then('User submits the form', async function () {
    demoSitePOM.submitForm();
    await browser.sleep(3000);});

When('User user adds name: {string} and comment: {string}', async function (name, comment) {
    demoSitePOM.addNameAndComment();
    await browser.sleep(3000);});

Then('User adds country: {string}', async function (country) {
    demoSitePOM.addCountry(country);
    await browser.sleep(3000);});

Then('User adds state: {string}', async function (state) {
    demoSitePOM.addState(state);
    await browser.sleep(3000);});

Then('User moves one element from left box to right box', async function () {
    demoSitePOM.moveFromLeftToRight();
    await browser.sleep(3000);});

Then('User adds one element to right list box', async function () {
    demoSitePOM.addElementRight();
    await browser.sleep(3000);});

Then('User removes all elements from right list box', async function () {
    demoSitePOM.removeAllElements();
    await browser.sleep(3000);});

Then('User adds all elements to the right list box', async function () {
    demoSitePOM.addAllElements();
    await browser.sleep(3000);});

Then('User searches for person by name: {string}', async function (person) {
    demoSitePOM.findPerson(person);
    await browser.sleep(3000);});

Then('User generates 500 characters', async function () {
    demoSitePOM.generateCharacters();
    await browser.sleep(3000);});

Then('User Downloads file', async function () {
    demoSitePOM.downloadFile();
    await browser.sleep(3000);});

Then('User navigates to second page', async function () {
    demoSitePOM.navigateToSecondPage();
    await browser.sleep(3000);});

Then('User navigates to last page', async function () {
    demoSitePOM.navigateToLastPage();
    await browser.sleep(3000);});

Then('User searches for person: {string}', async function (person) {
    demoSitePOM.searchForPersonInTable(person);
    await browser.sleep(5000);
});

Then('User drags and drops all elements', async function () {
    demoSitePOM.dragAndDropElements();
    await browser.sleep(3000);});

