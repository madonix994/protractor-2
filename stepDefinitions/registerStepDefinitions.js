var { Given, Then, When } = require('cucumber');
var { setDefaultTimeout } = require('cucumber');
setDefaultTimeout(60 * 1000);
let registerPOM = require('../pages/registerPOM.js');


Given('User navigates to url: {string}', async function (url) {
    await browser.get(url);
});


Then('User enters full name {string} {string}', async function (firstName, lastName) {
    registerPOM.enterFirstName(firstName);
    registerPOM.enterLastName(lastName);
    await browser.sleep(1000);
});


Then('User enters {string} mail address', async function (email) {// 
    registerPOM.enterEmail(email);

});


Then('User enters {string} home address', async function (address) {// 
    registerPOM.enterAdress(address);
});


Then('User enters {string} phone number', async function (phone) {// 
    registerPOM.enterPhone(phone);
});


Then('User chose {string} gender', async function (gender) {// 
    registerPOM.choseGender(gender);
});


Then('User chose {string} hobbie', async function (hobbie) {// 
    registerPOM.choseHobbie(hobbie);
    await browser.sleep(2000);
});


Then('User enters {string} language', async function (language) {// 
    registerPOM.choseLanguage(language);
});


Then('User enters {string} skill', async function (skill) {// 
    registerPOM.choseSkill(skill);
});


Then('User enters {string} country', async function (country) {// 
    registerPOM.choseCountry(country);
});


Then('User chose {string} country', async function (selectedCountry) {// 
    registerPOM.enterCountry(selectedCountry);
});


Then('User chose date of birth {string} {string} {string}', async function (year, month, day) {// 
    registerPOM.choseBirthDate(year, month, day);
    await browser.sleep(2000);
});


Then('User enters {string} password and repeats', async function (password) {// 
    registerPOM.enterPasswordAndRepeat(password);
});


Then('User submits the registration form', async function () {// 
    registerPOM.clickSubmitBtn();
    await browser.sleep(5000);
});